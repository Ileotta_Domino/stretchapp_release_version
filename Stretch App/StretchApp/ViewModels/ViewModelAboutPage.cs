﻿using StretchApp.Helper;
using StretchApp.Helper.Interfaces;
using StretchApp.Parameters.Interfaces;
using StretchApp.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Diagnostics;
using System.IO;

namespace StretchApp.ViewModels
{
    class ViewModelAboutPage : ViewModelBase, IClosableViewModel
    {

        private readonly IinputProcessor _inputProcessor;
        private readonly ITupleProcessor _tupleProcessor;
        private readonly ICalculatorHelper _calculatorHelper;
        private readonly IConfigurationListHelper _configurationListHelper;

        public string ReleasedTo { get; set; }
        public RelayCommand ClosePageCommand { get; set; }
        public RelayCommand GoOpenInstructionsCommand { get; set; }
        public RelayCommand GoOpenLicenseCommand { get; set; }
        public event EventHandler CloseWindowEvent;
        public ViewModelAboutPage(IinputProcessor inputProcessor, ICalculatorHelper calculatorHelper, ITupleProcessor tupleProcessor, IConfigurationListHelper configurationListHelper)
        {
            _inputProcessor = inputProcessor;
            _calculatorHelper = calculatorHelper;
            _tupleProcessor = tupleProcessor;
            _configurationListHelper = configurationListHelper;

            ReleasedTo = getUserName();
            ClosePageCommand = new RelayCommand(ClosePageStart, canexecute);
            GoOpenInstructionsCommand = new RelayCommand(OpenInstruction, canexecute);
            GoOpenLicenseCommand = new RelayCommand(OpenLicense, canexecute);
            //   ClosePageCommand = new RelayCommand(CloseWin);
        }
        private bool canexecute(object parameter)
        {
            return true;
        }

        
         private void OpenInstruction(object parameter)
        {
            Process wordProcess = new Process();
       //     MessageBox.Show("Environment.CurrentDirectory");
            string gesturefile = Path.Combine(Environment.CurrentDirectory, @"Shrink_Calculator.pdf");
            wordProcess.StartInfo.FileName = gesturefile;
            wordProcess.StartInfo.UseShellExecute = true;
            wordProcess.Start();
            CloseWindowEvent(this, null);

        }


        private void OpenLicense(object parameter)
        {
            Process wordProcess = new Process();
            //     MessageBox.Show("Environment.CurrentDirectory");
            string gesturefile = Path.Combine(Environment.CurrentDirectory, @"Domino Shrink Tool software licence v2 (SRH).pdf");
            wordProcess.StartInfo.FileName = gesturefile;
            wordProcess.StartInfo.UseShellExecute = true;
            wordProcess.Start();
            CloseWindowEvent(this, null);

        }

        private string getUserName()
        {
            string res;
            string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            if (!File.Exists(@"c:\Domino\Shrink\AppFiles\SavedData.txt"))
            {

                res = "";
            }
            else
            {
                using (var reader = new StreamReader(@"c:\Domino\Shrink\AppFiles\SavedData.txt"))
                {
                    res = reader.ReadLine();
                }
            }
            return res;
        }
        private void ClosePageStart(object parameter)
        {

            CloseWindowEvent(this, null);
        }
        private void ClosePage(object parameter)
        {
            CloseWindowEvent(this, null);

        }

    }
}
