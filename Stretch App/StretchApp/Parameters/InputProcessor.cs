﻿using StretchApp.Parameters.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Parameters
{
    public class IInputProcessor : IinputProcessor
    {
        int input1;

        public int GetInput1()
        {
            return InputKeeper.InputValue1;
        }
        public void SetInput1(int inputToSet)
        {
            InputKeeper.InputValue1 = inputToSet;
        }
        public int GetInput2()
        {
            return InputKeeper.InputValue2;
        }
        public void SetInput2(int inputToSet)
        {
            InputKeeper.InputValue2 = inputToSet;
        }
        public string GetID()
        {
            return InputKeeper.ID;
        }
        public void SetID(string inputToSet)
        {
            InputKeeper.ID = inputToSet;
        }
        public double GetWidestCircumference()
        {
            return InputKeeper.WidestCircumferenceValue ;
        }
        public void SetWidestCircumference(double inputToSet)
        {
            InputKeeper.WidestCircumferenceValue = inputToSet;
        }

        public double GetInd1WidestCircumference()
        {
            return InputKeeper.Ind1WidestCircumferenceValue;
        }
        public void SetInd1WidestCircumference(double inputToSet)
        {
            InputKeeper.Ind1WidestCircumferenceValue = inputToSet;
        }
        public double GetInd2WidestCircumference()
        {
            return InputKeeper.Ind2WidestCircumferenceValue;
        }
        public void SetInd2WidestCircumference(double inputToSet)
        {
            InputKeeper.Ind2WidestCircumferenceValue = inputToSet;
        }
        public double GetInd3WidestCircumference()
        {
            return InputKeeper.Ind3WidestCircumferenceValue;
        }
        public void SetInd3WidestCircumference(double inputToSet)
        {
            InputKeeper.Ind3WidestCircumferenceValue = inputToSet;
        }
        public double GetInd4WidestCircumference()
        {
            return InputKeeper.Ind4WidestCircumferenceValue;
        }
        public void SetInd4WidestCircumference(double inputToSet)
        {
            InputKeeper.Ind4WidestCircumferenceValue = inputToSet;
        }
        public double GetInd5WidestCircumference()
        {
            return InputKeeper.Ind5WidestCircumferenceValue;
        }
        public void SetInd5WidestCircumference(double inputToSet)
        {
            InputKeeper.Ind5WidestCircumferenceValue = inputToSet;
        }
        public double GetNarrowestCircumferenceValue()
        {
            return InputKeeper.NarrowestCircumferenceValue;
        }
        public void SetNarrowestCircumferenceValue(double inputToSet)
        {
            InputKeeper.NarrowestCircumferenceValue = inputToSet;
        }
        public double GetInd1NarrowestCircumferenceValue()
        {
            return InputKeeper.Ind1NarrowestCircumferenceValue;
        }
        public void SetInd1NarrowestCircumferenceValue(double inputToSet)
        {
            InputKeeper.Ind1NarrowestCircumferenceValue = inputToSet;
        }
        public double GetInd2NarrowestCircumferenceValue()
        {
            return InputKeeper.Ind2NarrowestCircumferenceValue;
        }
        public void SetInd2NarrowestCircumferenceValue(double inputToSet)
        {
            InputKeeper.Ind2NarrowestCircumferenceValue = inputToSet;
        }
        public double GetInd3NarrowestCircumferenceValue()
        {
            return InputKeeper.Ind3NarrowestCircumferenceValue;
        }
        public void SetInd3NarrowestCircumferenceValue(double inputToSet)
        {
            InputKeeper.Ind3NarrowestCircumferenceValue = inputToSet;
        }
        public double GetInd4NarrowestCircumferenceValue()
        {
            return InputKeeper.Ind4NarrowestCircumferenceValue;
        }
        public void SetInd4NarrowestCircumferenceValue(double inputToSet)
        {
            InputKeeper.Ind4NarrowestCircumferenceValue = inputToSet;
        }
        public double GetInd5NarrowestCircumferenceValue()
        {
            return InputKeeper.Ind5NarrowestCircumferenceValue;
        }
        public void SetInd5NarrowestCircumferenceValue(double inputToSet)
        {
            InputKeeper.Ind5NarrowestCircumferenceValue = inputToSet;
        }
        public string GetName()
        {
            return InputKeeper.NameValue;
        }
        public void SetName(string inputToSet)
        {
            InputKeeper.NameValue = inputToSet;
        }
        public bool GetEnabledFlag()
        {
            return InputKeeper.EnabledFlagValue;
        }
        public void SetEnabledFlag(bool inputToSet)
        {
            InputKeeper.EnabledFlagValue = inputToSet;
        }
        public int GetResultValueValue()
        {
            return InputKeeper.ResultValueValue;
        }
        public void SetResultValueValue(int inputToSet)
        {
            InputKeeper.ResultValueValue = inputToSet;
        }
        public int GetInd1ResultValueValue()
        {
            return InputKeeper.Ind1ResultValueValue;
        }
        public void SetInd1ResultValueValue(int inputToSet)
        {
            InputKeeper.Ind1ResultValueValue = inputToSet;
        }
        public int GetInd2ResultValueValue()
        {
            return InputKeeper.Ind2ResultValueValue;
        }
        public void SetInd2ResultValueValue(int inputToSet)
        {
            InputKeeper.Ind2ResultValueValue = inputToSet;
        }
        public int GetInd3ResultValueValue()
        {
            return InputKeeper.Ind3ResultValueValue;
        }
        public void SetInd3ResultValueValue(int inputToSet)
        {
            InputKeeper.Ind3ResultValueValue = inputToSet;
        }
        public int GetInd4ResultValueValue()
        {
            return InputKeeper.Ind4ResultValueValue;
        }
        public void SetInd4ResultValueValue(int inputToSet)
        {
            InputKeeper.Ind4ResultValueValue = inputToSet;
        }
        public int GetInd5ResultValueValue()
        {
            return InputKeeper.Ind5ResultValueValue;
        }
        public void SetInd5ResultValueValue(int inputToSet)
        {
            InputKeeper.Ind5ResultValueValue = inputToSet;
        }

        public int GetTintValueValue()
        {
            return InputKeeper.TintValueValue;
        }
        public void SetTintValueValue(int inputToSet)
        {
            InputKeeper.TintValueValue = inputToSet;
        }

        public int GetInd1TintValueValue()
        {
            return InputKeeper.Ind1TintValueValue;
        }
        public void SetInd1TintValueValue(int inputToSet)
        {
            InputKeeper.Ind1TintValueValue = inputToSet;
        }

        public int GetInd2TintValueValue()
        {
            return InputKeeper.Ind2TintValueValue;
        }
        public void SetInd2TintValueValue(int inputToSet)
        {
            InputKeeper.Ind2TintValueValue = inputToSet;
        }
        public int GetInd3TintValueValue()
        {
            return InputKeeper.Ind3TintValueValue;
        }
        public void SetInd3TintValueValue(int inputToSet)
        {
            InputKeeper.Ind3TintValueValue = inputToSet;
        }
        public int GetInd4TintValueValue()
        {
            return InputKeeper.Ind4TintValueValue;
        }
        public void SetInd4TintValueValue(int inputToSet)
        {
            InputKeeper.Ind4TintValueValue = inputToSet;
        }
        public int GetInd5TintValueValue()
        {
            return InputKeeper.Ind5TintValueValue;
        }
        public void SetInd5TintValueValue(int inputToSet)
        {
            InputKeeper.Ind5TintValueValue = inputToSet;
        }
        public int GetStartLocationValue()
        {
            return InputKeeper.StartLocationValue;
        }
        public void SetStartLocationValue(int inputToSet)
        {
            InputKeeper.StartLocationValue = inputToSet;
        }        
        public int GetInd1StartLocationValue()
        {
            return InputKeeper.Ind1StartLocationValue;
        }
        public void SetInd1StartLocationValue(int inputToSet)
        {
            InputKeeper.Ind1StartLocationValue = inputToSet;
        }
        public int GetInd2StartLocationValue()
        {
            return InputKeeper.Ind2StartLocationValue;
        }
        public void SetInd2StartLocationValue(int inputToSet)
        {
            InputKeeper.Ind2StartLocationValue = inputToSet;
        }
        public int GetInd3StartLocationValue()
        {
            return InputKeeper.Ind3StartLocationValue;
        }
        public void SetInd3StartLocationValue(int inputToSet)
        {
            InputKeeper.Ind3StartLocationValue = inputToSet;
        }
        public int GetInd4StartLocationValue()
        {
            return InputKeeper.Ind4StartLocationValue;
        }
        public void SetInd4StartLocationValue(int inputToSet)
        {
            InputKeeper.Ind4StartLocationValue = inputToSet;
        }
        public int GetInd5StartLocationValue()
        {
            return InputKeeper.Ind5StartLocationValue;
        }
        public void SetInd5StartLocationValue(int inputToSet)
        {
            InputKeeper.Ind5StartLocationValue = inputToSet;
        }


        public double GetHeightUntilShrinkValue()
        {
            return InputKeeper.HeightUntilShrinkValue;
        }
        public void SetHeightUntilShrinkValue(double inputToSet)
        {
            InputKeeper.HeightUntilShrinkValue = inputToSet;
        }
        public double GetHeightOfContainerValue()
        {
            return InputKeeper.HeightOfContainerValue;
        }
        public void SetHeightOfContainerValue(double inputToSet)
        {
            InputKeeper.HeightOfContainerValue = inputToSet;
        }
        public double GetHeightOfContainerInd1Value()
        {
            return InputKeeper.HeightOfContainerInd1Value;
        }
        public void SetHeightOfContainerInd1Value(double inputToSet)
        {
            InputKeeper.HeightOfContainerInd1Value = inputToSet;
        }

        public double GetHeightOfContainerInd2Value()
        {
            return InputKeeper.Ind2HeightOfContainerValue;
        }
        public void SetHeightOfContainerInd2Value(double inputToSet)
        {
            InputKeeper.Ind2HeightOfContainerValue = inputToSet;
        }
        public double GetHeightOfContainerInd3Value()
        {
            return InputKeeper.Ind3HeightOfContainerValue;
        }
        public void SetHeightOfContainerInd3Value(double inputToSet)
        {
            InputKeeper.Ind3HeightOfContainerValue = inputToSet;
        }
        public double GetHeightOfContainerInd4Value()
        {
            return InputKeeper.Ind4HeightOfContainerValue;
        }
        public void SetHeightOfContainerInd4Value(double inputToSet)
        {
            InputKeeper.Ind4HeightOfContainerValue = inputToSet;
        }
        public double GetHeightOfContainerInd5Value()
        {
            return InputKeeper.Ind5HeightOfContainerValue;
        }
        public void SetHeightOfContainerInd5Value(double inputToSet)
        {
            InputKeeper.Ind5HeightOfContainerValue = inputToSet;
        }

        public double GetHeightUntilShrinkInd1Value()
        {
            return InputKeeper.HeightUntilShrinkInd1Value;
        }
        public void SetHeightUntilShrinkInd1Value(double inputToSet)
        {
            InputKeeper.HeightUntilShrinkInd1Value = inputToSet;
        }

        public double GetHeightUntilShrinkInd2Value()
        {
            return InputKeeper.Ind2HeightUntilShrinkValue;
        }
        public void SetHeightUntilShrinkInd2Value(double inputToSet)
        {
            InputKeeper.Ind2HeightUntilShrinkValue = inputToSet;
        }
        public double GetHeightUntilShrinkInd3Value()
        {
            return InputKeeper.Ind3HeightUntilShrinkValue;
        }
        public void SetHeightUntilShrinkInd3Value(double inputToSet)
        {
            InputKeeper.Ind3HeightUntilShrinkValue = inputToSet;
        }
        public double GetHeightUntilShrinkInd4Value()
        {
            return InputKeeper.Ind4HeightUntilShrinkValue;
        }
        public void SetHeightUntilShrinkInd4Value(double inputToSet)
        {
            InputKeeper.Ind4HeightUntilShrinkValue = inputToSet;
        }
        public double GetHeightUntilShrinkInd5Value()
        {
            return InputKeeper.Ind5HeightUntilShrinkValue;
        }
        public void SetHeightUntilShrinkInd5Value(double inputToSet)
        {
            InputKeeper.Ind5HeightUntilShrinkValue = inputToSet;
        }

        public bool GetIndent1IsSelectedValue()
        {
            return InputKeeper.Indent1IsSelectedValue;
        }
        public void SetIndent1IsSelectedValue(bool inputToSet)
        {
            InputKeeper.Indent1IsSelectedValue = inputToSet;
        }

        public bool GetIndent2IsSelectedValue()
        {
            return InputKeeper.Indent2IsSelectedValue;
        }
        public void SetIndent2IsSelectedValue(bool inputToSet)
        {
            InputKeeper.Indent2IsSelectedValue = inputToSet;
        }
        public bool GetIndent3IsSelectedValue()
        {
            return InputKeeper.Indent1IsSelectedValue;
        }
        public void SetIndent3IsSelectedValue(bool inputToSet)
        {
            InputKeeper.Indent1IsSelectedValue = inputToSet;
        }
        public bool GetIndent4IsSelectedValue()
        {
            return InputKeeper.Indent4IsSelectedValue;
        }
        public void SetIndent4IsSelectedValue(bool inputToSet)
        {
            InputKeeper.Indent4IsSelectedValue = inputToSet;
        }
        public bool GetIndent5IsSelectedValue()
        {
            return InputKeeper.Indent5IsSelectedValue;
        }
        public void SetIndent5IsSelectedValue(bool inputToSet)
        {
            InputKeeper.Indent5IsSelectedValue = inputToSet;
        }
        public string GetUnits()
        {
            return InputKeeper.UnitsSelectedValue;
        }

        public void SetUnits(string input)
        {
            InputKeeper.UnitsSelectedValue = input;
        }
        public string GetConfigurationFileValue()
        {
            return InputKeeper.ConfigurationFileValue;
        }
        public void SetConfigurationFileValue(string input)
        {
            InputKeeper.ConfigurationFileValue = input;
        }

        public string GetReleasedToValueValue()
        {
            return InputKeeper.ReleasedToValue;
        }
        public void SetReleasedToValueValue(string input)
        {
            InputKeeper.ReleasedToValue = input;
        }
    }
}
