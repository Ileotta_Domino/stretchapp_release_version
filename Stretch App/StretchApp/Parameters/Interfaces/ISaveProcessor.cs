﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Parameters.Interfaces
{
    public interface ISaveProcessor
    {
        string GetSaveName();
        void SetSaveName(string input);
        List<string> GetConfigurationList();
        void CreateList();
        void AddToConfigurationList(string input);
    }
}
