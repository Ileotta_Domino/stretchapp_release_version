﻿using StretchApp.Helper.Interfaces;
using StretchApp.Parameters;
using StretchApp.Parameters.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Helper
{
    public class ConfigurationListHelper : IConfigurationListHelper
    {
        ISaveProcessor _saveProcessor = new SaveProcessor();
        IinputProcessor _inputProcessor = new IInputProcessor();
        public void WriteActualConfiguration()
        {
            string WidestCircumference = "1";
            string NarrowestCircumference = "1";
            string HeightOfContainer = "1";
            string HeightUntilShrink = "1";
            string HeightOfContainerInd1 = "1";

            _saveProcessor.CreateList();

            if (_inputProcessor.GetWidestCircumference() != 1.00)
            {
                WidestCircumference = _inputProcessor.GetWidestCircumference().ToString();
                _saveProcessor.AddToConfigurationList("WidestCircumference," + WidestCircumference);
            }
            else { _saveProcessor.AddToConfigurationList("WidestCircumference," + "1"); }
            if (_inputProcessor.GetNarrowestCircumferenceValue() != 1.00)
            {
                NarrowestCircumference = _inputProcessor.GetNarrowestCircumferenceValue().ToString();
                _saveProcessor.AddToConfigurationList("NarrowestCircumference," + NarrowestCircumference);
            }
            else { _saveProcessor.AddToConfigurationList("NarrowestCircumference," + "1"); }
            if (_inputProcessor.GetHeightOfContainerValue() != 1.00)
            {
                HeightOfContainer = _inputProcessor.GetHeightOfContainerValue().ToString();
                _saveProcessor.AddToConfigurationList("HeightOfContainer," + HeightOfContainer);
            }
            else { _saveProcessor.AddToConfigurationList("HeightOfContainer," + "1"); }
            if (_inputProcessor.GetHeightUntilShrinkValue() != 1.00)
            {
                HeightUntilShrink = _inputProcessor.GetHeightUntilShrinkValue().ToString();
                _saveProcessor.AddToConfigurationList("HeightUntilShrink," + HeightUntilShrink);
            }
            else { _saveProcessor.AddToConfigurationList("HeightUntilShrink," + "1"); }
        }

        public void ReadConfigurationFile(string filename)
        {
            string[] words;
            List<string> _configFileCopy = new List<string>();
            foreach (string line in File.ReadLines(filename))
            {
                words = line.Split(',');
                if (words[0] == "WidestCircumference")
                {
                    _inputProcessor.SetWidestCircumference(Convert.ToInt32((string)words[1]));
                }
                if (words[0] == "NarrowestCircumference")
                {
                    _inputProcessor.SetNarrowestCircumferenceValue(Convert.ToInt32((string)words[1]));
                }
                if (words[0] == "HeightOfContainer")
                {
                    _inputProcessor.SetHeightOfContainerValue(Convert.ToInt32((string)words[1]));
                }
                if (words[0] == "HeightUntilShrink")
                {
                    _inputProcessor.SetHeightUntilShrinkValue(Convert.ToInt32((string)words[1]));
                }
            }
        }

        public void WriteListTofile(string filename)
        {
            string pathBis = filename;

            using (StreamWriter twBis = File.CreateText(pathBis))
                foreach (var elem in _saveProcessor.GetConfigurationList())
                {
                    twBis.WriteLine("{0}", elem);
                }
        }



        public string fromCharArrayToString(char[] input)
        {
            string result = string.Join("", input);
            return result;
        }
    }
}
