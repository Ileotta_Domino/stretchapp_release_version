﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace StretchApp.Helper
{
   public class PasswordCmdHelper
    {
        public event EventHandler CanExecuteChanged;
        public string Password { get; set; }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        private void Execute(object parameter)
        {
            PasswordBox boxPass = (PasswordBox)parameter;
            Password = boxPass.Password;
        }
    }
}
