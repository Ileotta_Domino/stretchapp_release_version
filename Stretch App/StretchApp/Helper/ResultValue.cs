﻿using StretchApp.Parameters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace StretchApp.Helper
{
    public class ResultValue : INotifyPropertyChanged
    {
        int _ResultValue = 0;
        int _ResultValueValue = 0;
        int _ResultValueToKeep=0;
        private readonly IInputProcessor _inputProcessor;

        public ResultValue(IInputProcessor inputProcessor)
        {
            _inputProcessor = inputProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._ResultValue = 42;
            }
        }

        public int ResultValueToShow
        {
            get { return _ResultValueValue; }
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_ResultValueValue != value)
                {
                    _ResultValueValue = value;
                    RaisePropertyChanged("_ResultValueValueToShow");
                }
            }
        }


        public int ResultValueToKeep
        {
            get
            {

                return _ResultValueToKeep = _inputProcessor.GetResultValueValue();
            }

            set
            {
                if (_ResultValueToKeep != value)
                {
                    _inputProcessor.SetResultValueValue(value);
                    RaisePropertyChanged("ResultValueToKeep");
                }
            }
        }

        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
