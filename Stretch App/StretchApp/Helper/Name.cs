﻿using StretchApp.Parameters;
using StretchApp.Parameters.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace StretchApp.Helper
{
    public class  Name : INotifyPropertyChanged
    {
        private readonly IinputProcessor _inputProcessor;

        string _NameValue;
        string _NameToKeep;

        public Name(IinputProcessor inputProcessor)
        {
            _inputProcessor = inputProcessor;
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                this._NameValue = "42";
            }
        }

        public string NameToShow
        {
            get { return _NameValue;}
            //  set { _rowNumber = value; OnPropertyChanged("RowValue"); }
            set
            {
                if (_NameValue != value)
                {
                    _NameValue = value;
                    RaisePropertyChanged("NameToShow");
                }
            }
        }


        public string NameToKeep
        {
            get
            {

                return _NameToKeep = _inputProcessor.GetName();
            }

            set
            {
                if (_NameToKeep != value)
                {
                    _inputProcessor.SetName(value);
                    RaisePropertyChanged("NameToKeep");
                }
            }
        }

        private void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            { PropertyChanged(this, new PropertyChangedEventArgs(prop)); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
