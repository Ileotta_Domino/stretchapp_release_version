﻿using StretchApp.Helper.Interfaces;
using StretchApp.Parameters;
using StretchApp.Parameters.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace StretchApp.Helper
{
    public class PasswordValidator : FrameworkElement
    {
     
        static IDictionary<PasswordBox, Brush> _passwordBoxes = new Dictionary<PasswordBox, Brush>();
        private static readonly IFileEncrypterHelper _fileEncrypterHelper = new FileEncrypterHelper();
        private static readonly IHashHelper _hashHelper = new HashHelper();

        private static readonly IinputProcessor _inputProcessor = new IInputProcessor();

        public static readonly DependencyProperty Box1Property = DependencyProperty.Register("Box1", typeof(PasswordBox), typeof(PasswordValidator), new PropertyMetadata(Box1Changed));
        public static readonly DependencyProperty Box2Property = DependencyProperty.Register("Box2", typeof(PasswordBox), typeof(PasswordValidator), new PropertyMetadata(Box2Changed));



        public  PasswordBox Box1
        {
            get { return (PasswordBox)GetValue(Box1Property); }
            set { SetValue(Box1Property, value); }
        }
        public PasswordBox Box2
        {
            get { return (PasswordBox)GetValue(Box2Property); }
            set { SetValue(Box2Property, value); }
        }
 

        private static void Box1Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //var pv = (PasswordValidator)d;
            //_passwordBoxes[pv.Box1] = pv.Box1.BorderBrush;
            //pv.Box2.LostFocus += (obj, evt) =>
            //{
            //    if (pv.Box2.Password != pv.Box1.Password)
            //    {
            //        pv.Box1.BorderBrush = new SolidColorBrush(Colors.Red);
            //        _inputProcessor.SetEnabledFlag(false);
            //    }
            //    else
            //    {
            //        PasswordKeeper.SettestStringstring(pv.Box1.Password);
            //        _fileEncrypterHelper.WritePwdToFile(pv.Box1.Password, @"c:\encrypt\temp.txt");
            //        _fileEncrypterHelper.PwdFileEncrypter();
            //        _inputProcessor.SetEnabledFlag(true);

            //        pv.Box2.BorderBrush = _passwordBoxes[pv.Box1];
            //    }
            //};
        }



        private static void Box2Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var pv = (PasswordValidator)d;
            _passwordBoxes[pv.Box2] = pv.Box2.BorderBrush;
            pv.Box2.LostFocus += (obj, evt) =>
            {
                if (pv.Box1.Password != pv.Box2.Password)
                {
                    pv.Box2.BorderBrush = new SolidColorBrush(Colors.Red);
                    _inputProcessor.SetEnabledFlag(false);
                }
                else
                {
                    PasswordKeeper.SettestStringstring(pv.Box2.Password);
                    _fileEncrypterHelper.WritePwdToFile(pv.Box2.Password, @"c:\Domino\Shrink\Key\temp.txt");


                    SecureString testString = new SecureString();
                    using (testString = pv.Box2.SecurePassword)
                    {
                        var Password = pv.Box2.Password;
                       var sal= _hashHelper.GetSalt(39);

                        if (!File.Exists(@"c:\Domino\Shrink\AppFiles\SavedInitData.txt"))
                        {
                            using (StreamWriter twhid = File.CreateText(@"c:\Domino\Shrink\AppFiles\SavedInitData.txt"))

                                twhid.WriteLine(sal);
                            FileInfo myFile = new FileInfo(@"c:\Domino\Shrink\AppFiles\SavedInitData.txt");
                            myFile.Attributes |= FileAttributes.Hidden;
                        }
                        else
                        {
                            FileInfo myFile = new FileInfo(@"c:\Domino\Shrink\AppFiles\SavedInitData.txt");
                            myFile.Attributes &= ~FileAttributes.Hidden;
                            File.WriteAllText(@"c:\Domino\Shrink\AppFiles\SavedInitData.txt", String.Empty);
                            using (StreamWriter twhid = File.CreateText(@"c:\Domino\Shrink\AppFiles\SavedInitData.txt"))

                                twhid.WriteLine(sal);
                            myFile.Attributes |= FileAttributes.Hidden;
                        }

                        _fileEncrypterHelper.WriteToPWH(_hashHelper.HashPassword(sal,Password));



                     //   _fileEncrypterHelper.WriteToPWH(_fileEncrypterHelper.SimpleEncrypter(Password));

                    }
                    // _fileEncrypterHelper.PwdFileEncrypter();
                    _inputProcessor.SetEnabledFlag(true);

                    pv.Box2.BorderBrush = _passwordBoxes[pv.Box2];

                    File.Delete(@"c:\Domino\Shrink\Key\temp.txt");

                }
            };
        }
    }
}
