﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Helper.Interfaces
{
    public interface IFileEncrypterHelper
    {
        void EncryptFile(string filename);
        void WritePwdToFile(string input, string path);
        void RemoveEncryption(string FileName);
        string ReadEncrline(string FileName);
     //   void EncryptFile(string inputFile, string outputFile, string skey);
    //    string DecryptFile(string inputFile, string skey);
        string PwdFileDecrypter();
        void PwdFileEncrypter();

        string ReadPwdFile();

        void PwdFileWithCodeEncrypter();
  void WriteToPWH(string input);
        string SimpleEncrypter(string input);


    }
}
