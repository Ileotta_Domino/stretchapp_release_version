﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Helper.Interfaces
{
    public interface IHashHelper
    {
        string GetMd5Hash(MD5 md5Hash, string input);
        bool VerifyMd5Hash(MD5 md5Hash, string input, string hash);
        string GetSalt(int saltSize);
        string HashPassword(string salt, string password);
      //  string EncryptUsingMD5(string inputStr);


    }
}
