﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Helper.Interfaces
{
    public interface IConfigurationListHelper
    {
        void WriteActualConfiguration();
        void WriteListTofile(string filename);
        void ReadConfigurationFile(string filename);
    }
}
