﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Helper.Interfaces
{
    public interface ICalculatorHelper
    {
        int CalulculatePercentage(double part, double total);
        int GetTint(int input);
        string GetTintToShow(int input);
        int CalulculateStartLocationPercentage(double part, double total);
    }
}
