﻿using Ninject.Modules;
using StretchApp.Helper;
using StretchApp.Helper.Interfaces;
using StretchApp.Parameters;
using StretchApp.Parameters.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StretchApp.Properties
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IinputProcessor>().To<IInputProcessor>();
            Bind<IEncrypterHelper>().To<EncrypterHelper>();
        }
    }
}
